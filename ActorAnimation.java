import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class ActorAnimation extends Actor
{
    private List<GreenfootImage> listImage = new ArrayList<GreenfootImage>();

    Actor actor;
    
    boolean stateAnimate = false;
    int animationFrameDelay = 30;
    long animationTimeLast;
    int animationFrameIndex = 0;
    int animationFrameDirection = 1;
    
    public ActorAnimation(Actor actor, String[] stringListImagePath){
        this.actor = actor;
        
        for (String stringImagePath : stringListImagePath) {
            this.listImage.add(new GreenfootImage(stringImagePath));
        }
    }       
    
    public void act() {
        this.doAnimate();
    }

    public void doAnimationTrigger(int direction) {
        this.stateAnimate = true;
        this.animationTimeLast = System.currentTimeMillis();    
        this.animationFrameDirection = direction;
        this.animationFrameIndex = 0;        
    }

    public void doAnimationTrigger() {
        this.doAnimationTrigger(1);
    }
    
    private void doAnimate() {
        if (this.stateAnimate) {
            long timeCurrent = System.currentTimeMillis();
            if (timeCurrent - this.animationTimeLast > this.animationFrameDelay) {
                this.animationTimeLast = timeCurrent;
    
                this.actor.setImage(this.listImage.get(this.animationFrameIndex));
                
                this.animationFrameIndex = this.animationFrameIndex + this.animationFrameDirection;
                
                if (this.animationFrameIndex < 0 || this.animationFrameIndex > this.listImage.size() - 1){
                    this.stateAnimate = false;
                }
            }
        }
    }
}
