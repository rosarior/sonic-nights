import greenfoot.*; 


public class ActorCameraText extends Actor
{
    static private GreenfootImage imageText;
    static String text = "";
    private boolean stateEnabled = false;

    public void act()
    {
        this.imageText = new GreenfootImage(this.text, 24, Color.WHITE, Color.BLACK);
        this.setImage(imageText);
        this.setLocation(650, 17);
        
        if (this.stateEnabled) {
            this.getImage().setTransparency(255);
        } else {
            this.getImage().setTransparency(0);
        }
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    public void hide() {
        this.stateEnabled = false;
    }

    public void show() {
        this.stateEnabled = true;
    }
}
