import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class ActorClock extends Actor
{
    long clockSecondsRemaining;
    int clockUpdateDelay;
    int clockUpdateDelta;
    long clockWinningTime;
    long timerLastUpdate;
    static private GreenfootImage text;
    boolean stateEnabled;
    
    public ActorClock() {
        this.clockSecondsRemaining = 0;
        this.clockWinningTime = 21600;
        this.clockUpdateDelay = 45000; // Update clock every 45 real seconds
        this.clockUpdateDelta = 3600;  // Add one hour to game clock every update
        this.timerLastUpdate = System.currentTimeMillis();
        
        boolean stateEnabled = false;
        //static private GreenfootImage text;        
        
    }
    
    public void doSetEnabled() {
        this.stateEnabled = true;
        this.timerLastUpdate = System.currentTimeMillis();
        this.doDisplayUpdate();
    }
    
    public void act()
    {
        this.doCheckDisplayUpdate();
    }
    
    private void doCheckDisplayUpdate() {
        long timerCurrent = System.currentTimeMillis();
        
        if ((timerCurrent > this.timerLastUpdate + this.clockUpdateDelay) && this.stateEnabled) {
            this.timerLastUpdate = timerCurrent;
            this.clockSecondsRemaining += this.clockUpdateDelta;

            this.doDisplayUpdate();
            
            if (this.clockSecondsRemaining == this.clockWinningTime) {
                Game game = (Game) getWorld();
                game.doMusicStop();
                game.doSetActiveGameState("state-game-win");
            }            
        }        
    }
    
    private void doDisplayUpdate() {
        long hours;
        long minutes;
        long seconds;
        String textHour;
        
        hours = (this.clockSecondsRemaining / 3600);
        minutes = (this.clockSecondsRemaining % 3600 / 60);
        seconds = (this.clockSecondsRemaining % 60);
        
        if (hours == 0) {
            textHour = "12";
        } else {
            textHour = String.format("%02d", hours);
        }
        
        this.text = new GreenfootImage(
            "Time " + textHour + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds) + " AM", 24, Color.WHITE, Color.BLACK
        );
        this.setImage(this.text);
        this.setLocation(180, 17);
    }        
}
