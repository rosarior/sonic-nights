import java.util.ArrayList;
import java.util.List;

import greenfoot.*;


public class ActorDoor extends Actor {
    private boolean stateKeyBoardPress;
    private String keyboardKey;
    
    private GreenfootSound soundPowerOn;
    private GreenfootSound soundPowerOff;
    private GreenfootSound soundNoPower;
    private GreenfootSound soundDoorClang;
    
    private List<GreenfootImage> imageList = new ArrayList<GreenfootImage>();

    boolean statePowerOn = false;
    
    boolean stateAnimate = false;
    int animationFrameDelay = 30;
    long animationTimeLast;
    int animationFrameIndex = 0;
    int animationFrameDirection = 1;

    int mapPosition;
    ActorDoorButton doorButton;
    
    long energyTimeLast;
    int energyUsage;
    int energyUsageMillis;
    
    World world;
            
    public ActorDoor(
        String keyboardKey, int mapPosition, ActorDoorButton doorButton, String stringSoundPowerOff, String stringSoundPowerOn,
        String stringSoundNoPower, String stringSoundDoorClang, String[] imageList, int energyUsage, int energyUsageMillis
    ) {
        
        this.soundPowerOff = new GreenfootSound(stringSoundPowerOff);
        this.soundPowerOn = new GreenfootSound(stringSoundPowerOn);
        this.soundNoPower = new GreenfootSound(stringSoundNoPower);        
        this.soundDoorClang = new GreenfootSound(stringSoundDoorClang);
        
        this.keyboardKey = keyboardKey;
        this.mapPosition = mapPosition;
        this.doorButton = doorButton;

        this.energyUsage = energyUsage;
        this.energyUsageMillis = energyUsageMillis;
        
        for (String imagePath : imageList) {
            this.imageList.add(new GreenfootImage(imagePath));
        }
        
        //this.world = getWorld();
    }
    
    public void act() {
        //World world = getWorld();
        this.world = getWorld();
        
        if (Greenfoot.isKeyDown(this.keyboardKey)) {
            if (!this.stateKeyBoardPress) {
               // This executes only if the keyboard state is false.
               this.stateKeyBoardPress = true;
               // Set the keyboard state to avoid running this multiple times.
               //world.getObjects(ActorLeftDoorButton.class).get(0).setColorRed();

               if (!this.stateAnimate) {
                   // Only change the state of the door if no animation is happening.
                   if (this.statePowerOn) {
                       this.doPowerOff();
                   } else {
                       this.doPowerOn();
                   }
               }               
            }
        } else {
            // If the key is not pressed, then reset the keyboard state.
            this.stateKeyBoardPress = false;
        }
        
        this.doUpdatePowerUsage();
        this.doAnimate();
    }
    
    public void doPowerOn() {
        //World world = getWorld();        

        if (this.world.getObjects(ActorPowerMeter.class).get(0).getEnergy() > 0) {
            this.doorButton.setStateBusy();

            this.stateAnimate = true;
            this.animationTimeLast = System.currentTimeMillis();         
            this.animationFrameDirection = 1;
            this.animationFrameIndex = 0;
           
            this.soundPowerOn.play(); 
            this.energyTimeLast = System.currentTimeMillis();         
        } else {
            this.doorButton.setStateBusy();
            this.soundNoPower.play(); 
            this.doorButton.setStateReady();
        }
    }
    
    public void doPowerOff() {
        if (this.statePowerOn) {
            this.doorButton.setStateBusy();

            this.stateAnimate = true;
            this.animationTimeLast = System.currentTimeMillis();         
            this.animationFrameDirection = -1;
            this.animationFrameIndex = this.imageList.size() - 1;
           
            this.soundPowerOff.play();
        }
    }    
    
    private void doUpdatePowerUsage() {
        //World world = getWorld();
        
        if (this.statePowerOn) {
            long timeCurrent = System.currentTimeMillis();
            if (timeCurrent - this.energyTimeLast > this.energyUsageMillis) {
                this.energyTimeLast = timeCurrent;
                this.world.getObjects(ActorPowerMeter.class).get(0).lowerEnergy(this.energyUsage);
            }
        }        
    }
    
    private void doAnimate(){
        if (this.stateAnimate) {
            long timeCurrent = System.currentTimeMillis();
            if (timeCurrent - this.animationTimeLast > this.animationFrameDelay) {
                this.animationTimeLast = timeCurrent;
    
                this.setImage(this.imageList.get(this.animationFrameIndex));
                
                this.animationFrameIndex = this.animationFrameIndex + this.animationFrameDirection;
                
                if (this.animationFrameIndex < 0 || this.animationFrameIndex > this.imageList.size() - 1){
                    this.stateAnimate = false;
                    this.statePowerOn = !this.statePowerOn;
                    this.doorButton.setStateReady();
                }
            }
        }
    }
    
    public void playSoundClang() {
        this.soundDoorClang.play();   
    }
    
}
