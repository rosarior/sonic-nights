import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ActorLeftDoorButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ActorDoorButton extends Actor
{
    //World w;

    //private boolean stateKeyBoardPress, stateLeftDoorButtonPress;

    private GreenfootImage imageReady, imageBusy;

    public ActorDoorButton(String imagePathReady, String imagePathBusy)
    {
        this.imageReady = new GreenfootImage(imagePathReady);
        this.imageBusy = new GreenfootImage(imagePathBusy);    
        
        this.setStateReady();
        //stateLeftDoorButtonPress = false;
        //stateLeftDoorButtonPress = false;
    }

    /**
     * Act - do whatever the ActorLeftDoorButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        //World world = getWorld();
 
    }
    
    public void setStateBusy() {
    setImage(this.imageBusy);           
    }
    
    public void setStateReady() {
    setImage(this.imageReady);           
    }
}
