import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import greenfoot.*;  

public class ActorGameState extends Actor
{   
    HashMap<String, Integer> hashKeyState = new HashMap<String, Integer>();

    public String name;
 
    Game game;// = (Game) getWorld();  
    List<Actor> actors = new ArrayList<Actor>();

    public ActorGameState(String name, Game game) {
        super();
        this.name = name;
        this.game = game;
    }
    
    public void act()
    {
        // Add your action code here.
    }

    public void addObject(Actor actor, int positionX, int positionY) {
        this.game.addObject(actor, positionX, positionY);
        this.actors.add(actor);
    }
    
    public void doSetActiveGameState(String name) {
        Game game = (Game) getWorld();  

        game.doSetActiveGameState(name);    
    }
    
    public void doSetAsActive() {
        
    }
    
    public boolean doCheckKey(String key) {
        if (Greenfoot.isKeyDown(key)) {
            if (this.hashKeyState.getOrDefault(key, 0) == 0) {
                this.hashKeyState.put(key, 1);
                return true;
            }
        } else {
           this.hashKeyState.put(key, 0);
           return false;
        }
        
        return false;
    }    
    
    public void removeObject(Actor actor) {
        this.game.removeObject(actor);
    }
    
    public void DoSetNotActive() {       
        for (Actor actor : this.actors) {
            this.game.removeObject(actor);
        }
        this.actors.clear();
    }    
}
