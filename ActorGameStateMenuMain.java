import greenfoot.*;


public class ActorGameStateMenuMain extends ActorGameState {
    static private GreenfootImage imageBackground;

    long timeLast;
    int timeActivationDelay;
    long timerPressedStart;
    
    ActorText actorTextGameStart = new ActorText(this);
    
    public ActorGameStateMenuMain(Game game) {
        super("menu-main", game);
        
        //this.actorTextGameStart = new ActorText();
        //this.actorTextGameStart.doSetText("ASD");
        //this.actorTextGameStart.doShow();
        
        
        //World world = getWorld();
        //game.addObject(this.actorTextGameStart, 0 ,0);
        
        //Game game = (Game) getWorld();
        //System.out.println(this);
        //game.addObject(this, 750, 350);//this.positionX, this.positionY);
        
        //ActorText actorTextGameStart = new ActorText(this);
        actorTextGameStart.doSetText("Game start");
        actorTextGameStart.doSetPosition(650, 550);
        //actorTextGameStart.doShow();
        //System.out.println(game);
        //game.addObject(actorText, 650, 350);
        
        this.imageBackground = new GreenfootImage("images/states/menu-main.png");

        //this.imageTextStart = new GreenfootImage("Game Start", 24, Color.WHITE, Color.BLACK);
        
        this.timeActivationDelay = 1000;
        this.timeLast = System.currentTimeMillis(); 
    }
    
    public void act()
    {
        if (this.doCheckKey("enter")) {
            this.doSetActiveGameState("mode-survival");
        }
        /*
        long timeCurrent = System.currentTimeMillis();
        
        if (timeCurrent > this.timeLast + this.timeActivationDelay) {
            this.timeLast = timeCurrent;
            
            System.out.println("active: " + timeCurrent + ":" + this.name);
            //this.doSetActiveGameState("mode-survival");

        };
        // Add your action code here.
        */ 
    }
    /*
    protected void addedToWorld(World world) {
        this.timeLast = System.currentTimeMillis();
        
        this.setImage(this.imageBackground);
        this.actorTextGameStart.doShow();
    }
    */
    public void doSetAsActive() {
        this.timeLast = System.currentTimeMillis();
        
        this.setImage(this.imageBackground);
        this.actorTextGameStart.doShow();
        this.game.doMusicPlay();
    }

        
}
