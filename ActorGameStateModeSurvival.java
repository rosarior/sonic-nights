import java.util.ArrayList;
import java.util.List;

import greenfoot.*; 


public class ActorGameStateModeSurvival extends ActorGameState
{
    public ActorGameStateModeSurvival(Game game) {
        super("mode-survival", game);
     
    }

    protected void addedToWorld(World world) {
    }
    
    public void act()
    {
    }
    
    public void doSetAsActive() {
        GreenfootSound soundSonicNights = new GreenfootSound("sounds/sonic-nights.mp3");
        soundSonicNights.play();
        
        ActorClock actorClock = new ActorClock();
        addObject(actorClock, 650, 350);
        actorClock.doSetEnabled();
        
        Nightroom room = new Nightroom();
        addObject(room, 650, 350);
        
        Void dark = new Void();
        addObject(dark, 650, 350);
        
        ActorScreenNoise actorScreenNoise = new ActorScreenNoise();
        addObject(actorScreenNoise, 650, 350);
        
        ActorCameraText cameraText = new ActorCameraText();
        addObject(cameraText, 650, 350);        
         
        String[] imagePathListTabletAnimation = {
            "images/tablets/tablet-poweron-00.png", "images/tablets/tablet-poweron-01.png", "images/tablets/tablet-poweron-02.png",
            "images/tablets/tablet-poweron-03.png", "images/tablets/tablet-poweron-04.png", "images/tablets/tablet-poweron-05.png",
            "images/tablets/tablet-poweron-06.png", "images/tablets/tablet-poweron-07.png", "images/tablets/tablet-poweron-08.png"
        };
        
        ActorTablet actorTablet = new ActorTablet(
            "w", "q", "e", imagePathListTabletAnimation, "sounds/tablets/power-on.mp3", "sounds/tablets/power-off.mp3",
            "sounds/tablets/no-power.mp3", 2, 1000, cameraText
        );
        addObject(actorTablet, 650, 350);
        
        ActorPowerMeter powerMeter = new ActorPowerMeter();
        addObject(powerMeter, 650, 350);     
       
        // Door buttons
     
        ActorDoorButton actorDoorLeftButton = new ActorDoorButton("images/buttons/button-left-ready.png", "images/buttons/button-left-busy.png");
        addObject(actorDoorLeftButton, 52, 386);

        ActorDoorButton actorDoorMiddleButton = new ActorDoorButton("images/buttons/button-middle-ready.png", "images/buttons/button-middle-busy.png");
        addObject(actorDoorMiddleButton, 584, 211);       
       
        ActorDoorButton actorDoorRightButton = new ActorDoorButton("images/buttons/button-right-ready.png", "images/buttons/button-right-busy.png");
        addObject(actorDoorRightButton, 1249, 386);        

        // Left door
        
        String[] doorLeftImagePathList = {
            "images/doors/door-left-00.png", "images/doors/door-left-01.png", "images/doors/door-left-02.png",
            "images/doors/door-left-03.png", "images/doors/door-left-04.png", "images/doors/door-left-05.png"
        };
        ActorDoor actorDoorLeft = new ActorDoor(
            "a", 20, actorDoorLeftButton, "sounds/doors/door-open.mp3", "sounds/doors/door-close.mp3", "sounds/doors/no-power.mp3",
            "sounds/doors/door-clang.mp3", doorLeftImagePathList, 5, 1000
        );
        addObject(actorDoorLeft, 650, 350);

        // Vent
        String[] doorCenterImagePathList = {
            "images/doors/door-center-00.png", "images/doors/door-center-01.png", "images/doors/door-center-02.png",
            "images/doors/door-center-03.png", "images/doors/door-center-04.png"
        };
        ActorDoor actorDoorCenter = new ActorDoor(
            "s", 18, actorDoorMiddleButton, "sounds/doors/door-open.mp3", "sounds/doors/door-close.mp3", "sounds/doors/no-power.mp3", 
            "sounds/doors/door-clang.mp3", doorCenterImagePathList, 5, 1000
        );
        addObject(actorDoorCenter, 650, 350);
        
        // Right door
        String[] doorRightImagePathList = {
            "images/doors/door-right-00.png", "images/doors/door-right-01.png", "images/doors/door-right-02.png",
            "images/doors/door-right-03.png", "images/doors/door-right-04.png", "images/doors/door-right-05.png"
        };
        
        ActorDoor actorDoorRight = new ActorDoor(
            "d", 22, actorDoorRightButton, "sounds/doors/door-open.mp3", "sounds/doors/door-close.mp3", "sounds/doors/no-power.mp3", 
            "sounds/doors/door-clang.mp3", doorRightImagePathList, 5, 1000
        );
        addObject(actorDoorRight, 650, 350);
   
        // Sonic
        int[] pathSonic = {0, 3, 7, 11, 10, 13, 16, 20, 21}; // Short movement
        String[] soundSeen = {
            "sounds/animatronics/seen-1.mp3", "sounds/animatronics/seen-2.mp3", "sounds/animatronics/seen-3.mp3",
            "sounds/animatronics/seen-4.mp3"
        };
        
        List<AnimatronicImage> animatronicImagesSonic = new ArrayList<AnimatronicImage>();
        animatronicImagesSonic.add(new AnimatronicImage(20, "images/animatronics/sonic/sonic-20-20.png", 432, 485));  // close
        animatronicImagesSonic.add(new AnimatronicImage(20, "images/animatronics/sonic/sonic-20-16.png", 830, 300));  // middle
        animatronicImagesSonic.add(new AnimatronicImage(20, "images/animatronics/sonic/sonic-20-13.png", 846, 164));  // far
        animatronicImagesSonic.add(new AnimatronicImage(7, "images/animatronics/sonic/sonic-07-03.png", 1132, 626));  // close
        animatronicImagesSonic.add(new AnimatronicImage(7, "images/animatronics/sonic/sonic-07-07.png", 1013, 343));  // middle
        animatronicImagesSonic.add(new AnimatronicImage(7, "images/animatronics/sonic/sonic-07-11.png", 877, 71));  // far

        Animatronic animatronicSonic = new Animatronic(
            "Sonic", pathSonic, 30, 2000, animatronicImagesSonic,
            "images/animatronics/sonic/sonic-jumpscare.png", 
            soundSeen, "sounds/animatronics/animatronic-walking-01.mp3", "sounds/animatronics/sonic/sonic-jumpscare.mp3"
        );
        addObject(animatronicSonic, 0, 0);   
        
        
        //int[] pathTails = {0, 3, 7, 8, 12, 15, 19, 22, 21};
        int[] pathTails = {0, 3, 7, 8, 12, 15, 19, 22, 21};
        List<AnimatronicImage> animatronicImagesTails = new ArrayList<AnimatronicImage>();
        animatronicImagesTails.add(new AnimatronicImage(22, "images/animatronics/tails/tails-22-22.png", 680, 375));
        animatronicImagesTails.add(new AnimatronicImage(19, "images/animatronics/tails/tails-19-19.png", 540, 649));  // close
        animatronicImagesTails.add(new AnimatronicImage(19, "images/animatronics/tails/tails-19-15.png", 629, 319));  // middle
        animatronicImagesTails.add(new AnimatronicImage(19, "images/animatronics/tails/tails-19-12.png", 708, 79));  // far
        animatronicImagesTails.add(new AnimatronicImage(7, "images/animatronics/tails/tails-07-03.png", 311, 649));  // close
        animatronicImagesTails.add(new AnimatronicImage(7, "images/animatronics/tails/tails-07-07.png", 87, 231));  // middle

        Animatronic animatronicTails = new Animatronic(
            "Tails", pathTails, 30, 2000, animatronicImagesTails,
            "images/animatronics/tails/tails-jumpscare.png", 
            soundSeen, "sounds/animatronics/animatronic-walking-01.mp3", "sounds/animatronics/tails/tails-jumpscare.mp3"
        );
        addObject(animatronicTails, 0, 0);  
       
        
        int[] pathKnuckles = {0, 3, 7, 11, 14, 17, 18, 21};  // Short path
        List<AnimatronicImage> animatronicImagesKnuckles = new ArrayList<AnimatronicImage>();
        animatronicImagesKnuckles.add(new AnimatronicImage(14, "images/animatronics/knuckles/knuckles-14-14.png", 1090, 529));  // Closet
        animatronicImagesKnuckles.add(new AnimatronicImage(18, "images/animatronics/knuckles/knuckles-18-17.png", 747, 311));  // Vent far
        animatronicImagesKnuckles.add(new AnimatronicImage(18, "images/animatronics/knuckles/knuckles-18-18.png", 1046, 438));  // Vent close
        animatronicImagesKnuckles.add(new AnimatronicImage(7, "images/animatronics/knuckles/mighty-07-03.png", 590, 621));  // Dinning close
        animatronicImagesKnuckles.add(new AnimatronicImage(7, "images/animatronics/knuckles/mighty-07-07.png", 562, 374));  // Dinning middle
        animatronicImagesKnuckles.add(new AnimatronicImage(7, "images/animatronics/knuckles/mighty-07-11.png", 504, 87));  // Dinning far

        Animatronic animatronicKnuckles = new Animatronic(
            "Knuckles", pathKnuckles, 30, 2000, animatronicImagesKnuckles,
            "images/animatronics/knuckles/knuckles-jumpscare.png", 
            soundSeen, "sounds/animatronics/animatronic-walking-01.mp3", "sounds/animatronics/knuckles/knuckles-jumpscare.mp3"
        );
        addObject(animatronicKnuckles, 0, 0);          

    }    
    
    public void DoSetNotActive() {
        super.DoSetNotActive();
    }       
   
}
