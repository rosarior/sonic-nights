import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class ActorGameStateWin extends ActorGameState
{
    static private GreenfootImage imageBackground;
    static private GreenfootSound soundWin;

    public ActorGameStateWin(Game game) {
        super("state-game-win", game);
        this.imageBackground = new GreenfootImage("images/state-win.jpg");
        this.soundWin = new GreenfootSound("sounds/sound-win.mp3");
        this.setImage(this.imageBackground);
    }
    
    public void act()
    {
        // Add your action code here.
    }
    
    public void doSetAsActive() {
        //this.setImage(this.imageBackground);
        //this.actorTextGameStart.doShow();
        game.doMusicStop();
        this.soundWin.play();
    }    
}
