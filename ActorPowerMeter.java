import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import greenfoot.*;


public class ActorPowerMeter extends Actor
{
    NumberFormat formatPercent = NumberFormat.getPercentInstance();
            
    private double energyMaximum = 2000;
    private double energyCurrent = energyMaximum;
    private double energyCurrentPercent;
    boolean stateEnergyOver = false;
    
    static private GreenfootImage imageText;
    String stringTextOld = null;
    
    //List<GreenfootSound> soundHeartbearList = new ArrayList<GreenfootSound>();

    GreenfootSound[] soundHeartbearList = {
        new GreenfootSound("sounds/heartbeat-175.mp3"),
        new GreenfootSound("sounds/heartbeat-150.mp3"),
        new GreenfootSound("sounds/heartbeat-125.mp3"),
        new GreenfootSound("sounds/heartbeat-100.mp3")
    };
 
    GreenfootSound soundPowerDown = new GreenfootSound("sounds/power-down.mp3");
   
    private Boolean stateEnabled;
    private long soundHeartbearTimeLast = System.currentTimeMillis();
    private long soundHeartbeatDelayInitial = 4000;
    private long soundHeartbeatDelayCurrent = soundHeartbeatDelayInitial;  

    public ActorPowerMeter() {
        this.formatPercent.setMinimumFractionDigits(2);
        this.stateEnabled = true;
    }
            
    public void act()
    {
        if (this.stateEnabled) {
            this.energyCurrentPercent = this.energyCurrent / this.energyMaximum;
            this.doSetText("Power: " + this.formatPercent.format(this.energyCurrentPercent));
            
            long timeCurrent = System.currentTimeMillis();
            if (timeCurrent > this.soundHeartbearTimeLast + soundHeartbeatDelayCurrent) {
                this.soundHeartbearTimeLast = timeCurrent;
    
                this.soundHeartbearList[(int)(this.energyCurrentPercent / 51)].play();
            }
            
            this.soundHeartbeatDelayCurrent = (long)this.energyCurrent;
        }
    }
    
    private void doSetText(String text) {
        if (text != this.stringTextOld) {
            this.stringTextOld = text;
            this.imageText = new GreenfootImage(text, 24, Color.WHITE, Color.BLACK);
            this.setImage(imageText);
            this.setLocation(1147, 17);
        }
    }
    
    public double getEnergy() {
        return this.energyCurrent;
    }
    
    public void lowerEnergy(int amount) {
        //System.out.println("lowerEnergy " + amount + "=" + this.energyCurrent);
        this.energyCurrent = this.energyCurrent - amount;
        
        if (this.energyCurrent < 0) {
            this.energyCurrent = 0;
            this.doTriggerPowerDown();
        }
    }
    
    public void doDisable() {
        this.stateEnabled = false;
    }
    
    public void doTriggerPowerDown() {
        if (!this.stateEnergyOver) {
            World world = getWorld();

            this.stateEnergyOver = true;

            this.soundPowerDown.play();
                
            for (ActorDoor powerActor: world.getObjects(ActorDoor.class)) {
                powerActor.doPowerOff();
            }              
            for (ActorTablet powerActor: world.getObjects(ActorTablet.class)) {
                powerActor.doPowerOff();
            }      
            world.getObjects(Nightroom.class).get(0).doSetPowerOff();
        }
    }
}
