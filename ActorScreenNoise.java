import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class ActorScreenNoise extends Actor
{
    GreenfootImage[] imageList;
    //int animationFrameCurrent = 0;
    //int animationFrameTotal = 0;

    boolean stateAnimate = false;
    int animationFrameDelay = 50;
    long animationTimeLast;
    int animationFrameIndex = 0;
    int animationFrameDirection = 1;
    
    public ActorScreenNoise() {
        GreenfootImage[] imageList = {
             new GreenfootImage("images/noise/noise-transparent.png"),
             new GreenfootImage("images/noise/noise-solid-00.png"),
             new GreenfootImage("images/noise/noise-solid-01.png"),
             new GreenfootImage("images/noise/noise-solid-02.png"),
             new GreenfootImage("images/noise/noise-solid-00.png"),
             new GreenfootImage("images/noise/noise-transparent.png")
        };

        this.imageList = imageList;
        //this.animationFrameTotal = this.animationFrames.length;
        //this.imageNone = GreenfootImage)null);
    }
    
    public void act() {
        this.animate();
    }
    
    public void play() {
        this.stateAnimate = true;
        this.animationFrameIndex = 0;
    }
    
    public void animate() {
        /*
        for (int frame = 0;frame < this.imageList.length -1;frame += 1) {
            this.setImage(this.imageList[frame]);
            //this.getImage().setTransparency(0);
        }
        //this.getImage().setTransparency(255);
        this.setImage((GreenfootImage)null);
        */
        if (this.stateAnimate) {
            long timeCurrent = System.currentTimeMillis();
            if (timeCurrent - this.animationTimeLast > this.animationFrameDelay) {
                this.animationTimeLast = timeCurrent;
    
                this.setImage(this.imageList[this.animationFrameIndex]);
                
                this.animationFrameIndex = this.animationFrameIndex + this.animationFrameDirection;
                
                if (this.animationFrameIndex < 0 || this.animationFrameIndex > this.imageList.length - 1){
                    this.stateAnimate = false;
                    this.setImage((GreenfootImage)null);

                    //this.statePowerOn = !this.statePowerOn;
                    //this.doorButton.setStateReady();
                }
            }
        }        
    }
}
