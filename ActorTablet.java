import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import greenfoot.*;


class Camera {
   public String path;
   public GreenfootImage image;
   public int position;
   public String label;
   int[] positionListVisible;
   
   public Camera(String path, int position, String label, int[] positionListVisible) {
       this.path = path;
       this.position = position;
       this.label = label;
       this.image = new GreenfootImage(path);
       this.positionListVisible = positionListVisible;
   }
}

public class ActorTablet extends Actor
{
    String keyboardKeyToggle;
    String keyboardKeyCameraNext;
    String keyboardKeyCameraPrevious;

    int cameraListIndex = 0;
    Camera[] cameras;
    Camera cameraCurrent;

    List<GreenfootImage> imageListPowerOn = new ArrayList<GreenfootImage>();

    private GreenfootSound soundPowerOn;
    private GreenfootSound soundPowerOff;
    private GreenfootSound soundNoPower;
    //private GreenfootSound soundCameraNoiseLoop = new GreenfootSound("sounds/tablets/camera-noise-loop.mp3");
    private GreenfootSound soundCameraSwitch = new GreenfootSound("sounds/tablets/camera-switch.mp3");
    
    boolean statePowerOn = false;
    boolean stateKeyBoardPress = false;

    boolean stateAnimate = false;
    int animationFrameDelay = 50;
    long animationTimeLast;
    int animationFrameIndex = 0;
    int animationFrameDirection = 1;

    long energyTimeLast;
    int energyUsage;
    int energyUsageMillis;

    HashMap<String, Integer> hashKeyState = new HashMap<String, Integer>();
    
    ActorCameraText cameraText;
   
    public ActorTablet(
        String keyboardKeyToggle, String keyboardKeyCameraPrevious, String keyboardKeyCameraNext, String[] imagePathListTabletAnimation,
        String stringSoundPowerOn, String stringSoundPowerOff, String stringSoundNoPower,
        int energyUsage, int energyUsageMillis, ActorCameraText cameraText 
    ) {
        this.keyboardKeyToggle = keyboardKeyToggle;
        this.keyboardKeyCameraPrevious = keyboardKeyCameraPrevious;
        this.keyboardKeyCameraNext = keyboardKeyCameraNext;

        for (String imagePathTabletAnimationFrame : imagePathListTabletAnimation) {
            this.imageListPowerOn.add(new GreenfootImage(imagePathTabletAnimationFrame));
        }        
        
        int[] cameraMainAreaVisiblePositions = {3, 7, 11};
        int[] cameraClosetVisiblePositions = {14};
        int[] cameraVentVisiblePositions = {17, 18};
        int[] cameraLeftVisiblePositions = {20, 16, 13};
        int[] cameraRightEntryVisiblePositions = {19, 15, 12};
        int[] cameraRightVisiblePositions = {22};
        
        Camera[] cameras = {
            new Camera("camera/camera-01.png", 7, "1 - Main area", cameraMainAreaVisiblePositions),
            new Camera("camera/camera-04.png", 14, "4 - Storage closet", cameraClosetVisiblePositions),
            new Camera("camera/camera-05.png", 18, "5 - Vent", cameraVentVisiblePositions),
            new Camera("camera/camera-06.png", 20, "6 - Left hallway", cameraLeftVisiblePositions),            
            new Camera("camera/camera-07.png", 19, "7 - Right hallway entry", cameraRightEntryVisiblePositions),            
            new Camera("camera/camera-08.png", 22, "8 - Right hallway", cameraRightVisiblePositions)
        };
        this.cameras = cameras;
               
        this.soundPowerOff = new GreenfootSound(stringSoundPowerOff);
        this.soundPowerOn = new GreenfootSound(stringSoundPowerOn);
        this.soundNoPower = new GreenfootSound(stringSoundNoPower);
        
        this.energyUsage = energyUsage;
        this.energyUsageMillis = energyUsageMillis;      
        
        this.cameraText = cameraText;
    }
    
    private boolean checkKey(String key) {
        if (Greenfoot.isKeyDown(key)) {
            if (this.hashKeyState.getOrDefault(key, 0) == 0) {
                this.hashKeyState.put(key, 1);
                return true;
            }
        } else {
           this.hashKeyState.put(key, 0);
           return false;
        }
        
        return false;
    }
    
    public void act() 
    {
        World world = getWorld();
        
        if (this.checkKey(this.keyboardKeyToggle)) {
           if (!this.stateAnimate) {
               // Only change the state of the door if no animation is happening.
               if (this.statePowerOn) {
                   this.doPowerOff();
               } else {
                   this.doPowerOn();
               }
           }               
        } else if (this.checkKey(this.keyboardKeyCameraNext)) {
           if (this.statePowerOn) {
                world.getObjects(ActorScreenNoise.class).get(0).play();
                this.soundCameraSwitch.play();
                cameraListIndex ++;
                if (cameraListIndex > cameras.length - 1) {
                    cameraListIndex = 0;
                }
           }
        } else if (this.checkKey(this.keyboardKeyCameraPrevious)) {
            if (this.statePowerOn) {
                world.getObjects(ActorScreenNoise.class).get(0).play();
                this.soundCameraSwitch.play();
                cameraListIndex --;
                if (cameraListIndex < 0) {
                    cameraListIndex =  cameras.length - 1;
                }
            }
        }        
 
        this.updatePowerUage();
        this.doAnimate(); 
        this.doCameraDisplayUpdate();
    }

    public void checkAnimatronicMove(Animatronic actorAnimatronic) {
        World world = getWorld();
        
        if (this.statePowerOn && !this.stateAnimate) {
            this.cameraCurrent = this.cameras[this.cameraListIndex];
        
            for (int cameraVisiblePositionIndex=0; cameraVisiblePositionIndex < this.cameraCurrent.positionListVisible.length; cameraVisiblePositionIndex++) {
                int cameraVisiblePosition = this.cameraCurrent.positionListVisible[cameraVisiblePositionIndex];    
                if (actorAnimatronic.positionCurrent == cameraVisiblePosition || actorAnimatronic.positionPrevious == cameraVisiblePosition) {
                    world.getObjects(ActorScreenNoise.class).get(0).play();
                }
            }
        }
    }
    
    private void doCameraDisplayUpdate() {
        World world = getWorld();

        if (this.statePowerOn && !this.stateAnimate) {
            this.cameraCurrent = this.cameras[this.cameraListIndex];
            this.cameraText.setText("Camera: " + cameraCurrent.label);
            this.cameraText.show();
            this.setImage(this.cameras[this.cameraListIndex].image);
           
            boolean stateAnimatronicVisible = false;
        
            for (Animatronic animatronic: world.getObjects(Animatronic.class)) {
                stateAnimatronicVisible = false;
                
                for (int cameraVisiblePositionIndex=0; cameraVisiblePositionIndex < this.cameraCurrent.positionListVisible.length; cameraVisiblePositionIndex++) {
                    int cameraVisiblePosition = this.cameraCurrent.positionListVisible[cameraVisiblePositionIndex];
                    if (animatronic.positionCurrent == cameraVisiblePosition) {
                        animatronic.setAnimatronicImage(this.cameraCurrent.position, cameraVisiblePositionIndex);
                        stateAnimatronicVisible = true;
                    }
                }                
                if (!stateAnimatronicVisible) {
                    animatronic.hide();
                }
             };
        } else {
            this.cameraText.hide();
        }
    }
    
    private void doAnimate(){
        if (this.stateAnimate) {
            long timeCurrent = System.currentTimeMillis();
            if (timeCurrent - this.animationTimeLast > this.animationFrameDelay) {
                this.animationTimeLast = timeCurrent;
    
                this.setImage(this.imageListPowerOn.get(this.animationFrameIndex));
                
                this.animationFrameIndex = this.animationFrameIndex + this.animationFrameDirection;
                
                if (this.animationFrameIndex < 0 || this.animationFrameIndex > this.imageListPowerOn.size() - 1){
                    this.stateAnimate = false;
                    this.statePowerOn = !this.statePowerOn;
                }
            }
        }
    }
    
    private void updatePowerUage() {
        World world = getWorld();
        
        if (this.statePowerOn) {
            long timeCurrent = System.currentTimeMillis();
            if (timeCurrent - this.energyTimeLast > this.energyUsageMillis) {
                this.energyTimeLast = timeCurrent;
                world.getObjects(ActorPowerMeter.class).get(0).lowerEnergy(this.energyUsage);
            }
        }  
    }
    
    public void doPowerOn() {
        World world = getWorld();        

        if (world.getObjects(ActorPowerMeter.class).get(0).getEnergy() > 0) {
            this.stateAnimate = true;
            this.animationTimeLast = System.currentTimeMillis();         
            this.animationFrameDirection = 1;
            this.animationFrameIndex = 0;
            this.energyTimeLast = System.currentTimeMillis();    
            this.soundPowerOn.play();       
            //this.soundCameraNoiseLoop.playLoop();
        } else {
            this.soundNoPower.play(); 
        }
    }
    
    public void doPowerOff() {
       if (this.statePowerOn) {
           World world = getWorld();
           
           this.stateAnimate = true;
           this.animationTimeLast = System.currentTimeMillis();         
           this.animationFrameDirection = -1;
           this.animationFrameIndex = this.imageListPowerOn.size() - 1;
           //this.soundCameraNoiseLoop.stop();
           this.soundPowerOff.play();   
           
           
           for (Animatronic animatronic: world.getObjects(Animatronic.class)) {
               animatronic.hide();
           }
        }
    }
    
    public void hide() {
        this.setImage((GreenfootImage)null);
    }
}