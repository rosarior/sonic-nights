import greenfoot.*;

public class ActorText extends Actor
{
    private GreenfootImage imageText;
    String text = "";
    Boolean stateEnabled = false;
    int positionX = 650;
    int positionY = 350;

    ActorGameState actorGameState;
    //Actor self;
    
    public ActorText(ActorGameState actorGameState) {
        super();
        
        this.actorGameState = actorGameState;
    }

    public void act() {
        //this.game = (Game) getWorld();
        
        if (this.stateEnabled) {
            //this.setImage(imageText);
            //this.getImage().setTransparency(255);
        } else {
            //this.getImage().setTransparency(0);
            //this.setImage((GreenfootImage)null);
        }        
    }

    public void doHide() {
        this.stateEnabled = false;
        this.actorGameState.removeObject(this);
        this.setImage((GreenfootImage)null);        
    }

    public void doShow() {
        this.stateEnabled = true;
        
        this.actorGameState.addObject(this, this.positionX, this.positionY);        
        this.setImage(imageText);        
    }   
    
    public void doSetPosition(int positionX, int positionY) {
        this.positionX = positionX;
        this.positionY = positionY;   
        
        this.setLocation(this.positionX, this.positionY);        
    }
    
    public void doSetText(String text) {
        this.text = text;
        this.imageText = new GreenfootImage(this.text, 24, Color.WHITE, Color.BLACK);        
    } 
}
