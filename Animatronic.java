import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import greenfoot.*; 

public class Animatronic extends Actor
{
    Random random = new Random();

    int positionCurrent;
    int positionPrevious;
    
    int[] positionPath = {};
    int movementChance = 1;
    int positionPathIndex = 0;
    String name = null;
    
    long timeLast;
    int timeActivationDelay;
    
    boolean stateSeen = false;
    boolean stateEnabled = true;
    
    GreenfootImage imageJumpScare;
    public GreenfootSound soundJumpScare;
    private GreenfootSound soundMovement;
    List<GreenfootSound> soundSeen = new ArrayList<GreenfootSound>();
    Actor actorDoor;
    List<AnimatronicImage> animatronicImages = new ArrayList<AnimatronicImage>();
    
    int intSoundSeenPlayChance;
    int intSoundMovementPlayChance;
    
    public Animatronic(
        String name, int[] positionPath, int movementChance, int timeActivationDelay, List<AnimatronicImage> animatronicImages,
        String imageJumpScare, String[] soundSeen, String stringPathSoundMovement, String soundJumpScare
    ) {        
        this.name = name;
        this.positionPath = positionPath;
        this.movementChance = movementChance;

        this.timeActivationDelay = timeActivationDelay;
        this.timeLast = System.currentTimeMillis(); 
        
        this.positionCurrent = this.positionPath[0];
        this.positionPrevious = this.positionPath[0];
        
        this.imageJumpScare = new GreenfootImage(imageJumpScare);
        
        this.actorDoor = actorDoor;
        this.animatronicImages = animatronicImages;
        
        for (String soundPath : soundSeen) {
            this.soundSeen.add(new GreenfootSound(soundPath));
        }
        this.soundJumpScare = new GreenfootSound(soundJumpScare);
        this.soundMovement = new GreenfootSound(stringPathSoundMovement);

        this.stateEnabled = true;
        
        this.intSoundMovementPlayChance = 50;
        this.intSoundSeenPlayChance = 50;
    }
    
    public void act()
    {
        long timeCurrent = System.currentTimeMillis();
        
        if ((timeCurrent > this.timeLast + this.timeActivationDelay) && this.stateEnabled) {
            this.timeLast = timeCurrent;
            //System.out.println("[" + this.name + "] Activation.");
            //System.out.println("My name is: " + this.name);    

            int int_random = random.nextInt(100); 
            //System.out.println("[" + this.name + "] random: " + int_random);

            if (int_random <= this.movementChance) {
                //System.out.println("[" + this.name + "] Move");
                this.executeMove();
            }
        }
    }
    
    public void doDisable() {
        this.stateEnabled = false;
    }
    
    private void executeMove() {
        World world = getWorld();
        int newPosition;

        this.positionPrevious = this.positionPath[this.positionPathIndex];

        this.positionPathIndex++;       
        /*
        if (!this.stateSeen) {
            if (random.nextInt(100) <= this.intSoundMovementPlayChance) {                //
                //soundSeen.get(random.nextInt(soundSeen.size())).play();
                this.soundMovement.play();
            }
        }
        */
        for (ActorDoor door: world.getObjects(ActorDoor.class)) {
            if (this.positionCurrent == door.mapPosition) {
                if (door.statePowerOn) {
                    //System.out.println("[" + this.name + "] bumped with door.");
                    this.positionPathIndex = 0;
                    door.playSoundClang();
                }
            }
        }
        
        this.positionCurrent = this.positionPath[this.positionPathIndex];
        //System.out.println("[" + this.name + "] position is " + this.positionCurrent);

        //world.getObjects(ActorScreenNoise.class).get(0).play();
        for (ActorTablet actorTablet: world.getObjects(ActorTablet.class)) {
            actorTablet.checkAnimatronicMove(this);
        }
        
        if (this.positionCurrent == 21) {
            this.jumpScare();
        }
    }
    
    public void jumpScare() {
        World world = getWorld();
        
        for (ActorTablet tablet: world.getObjects(ActorTablet.class)) {
            tablet.doPowerOff();
        }

        for (Animatronic animatronic: world.getObjects(Animatronic.class)) {
            animatronic.doDisable();
        }                    

        for (ActorPowerMeter actorPowerMeter: world.getObjects(ActorPowerMeter.class)) {
            actorPowerMeter.doDisable();
        }                    

        Game game = (Game) getWorld();
        game.doMusicStop();
        this.setImageJumpScare();
        this.soundJumpScare.play();
    }
    
    public void playSoundSeen() {
        if (!this.stateSeen) {
            this.stateSeen = true;
            
            if (random.nextInt(100) <= this.intSoundSeenPlayChance) {
                soundSeen.get(random.nextInt(soundSeen.size())).play();
            }
        }  
    }

    public void setImageJumpScare() {       
        this.setLocation(650, 350);
        this.setImage(this.imageJumpScare);        
        //this.getImage().setTransparency(255);
    } 
    
    public void hide() {
        this.setImage((GreenfootImage)null);
        //this.getImage().setTransparency(0);
        this.stateSeen = false;
    }

    public void setAnimatronicImage(int cameraPosition, int cameraVisiblePositionIndex) {
        for (AnimatronicImage animatronicImage : this.animatronicImages) {
            if (animatronicImage.cameraPosition == cameraPosition) {
                AnimatronicImage animatronicImageSelected = animatronicImages.get(
                    animatronicImages.indexOf(animatronicImage) + cameraVisiblePositionIndex
                );
                
                this.setImage(animatronicImageSelected.image);
                this.setLocation(
                    animatronicImageSelected.positionX,
                    animatronicImageSelected.positionY                
                );
                this.playSoundSeen();

              
                return;

            }
        }
        
    }    
}
