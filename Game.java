import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import greenfoot.*;

class AnimatronicImage {
   public GreenfootImage image;

   public int cameraPosition;
   public String path;
   public int positionX;
   public int positionY;
   
   public AnimatronicImage(int cameraPosition, String path, int positionX, int positionY) {
       this.cameraPosition = cameraPosition;
       this.image = new GreenfootImage(path);
       this.path = path;
       this.positionX = positionX;
       this.positionY = positionY;
   }
}

public class Game extends World
{
    public GreenfootSound soundBackgroundMusic = new GreenfootSound("sounds/background-music.mp3");
    public ActorScreenNoise actorScreenNoise;
        
    private HashMap<String, ActorGameState> hashActorGameStates = new HashMap<String, ActorGameState>();
    private String actorGameStateActive;
    
    public Game()
    {    
        super(1300, 700, 1); 
        
        
        setPaintOrder(
            ActorText.class, ActorGameState.class, 
            ActorClock.class, ActorCameraText.class, ActorPowerMeter.class, ActorScreenNoise.class, Animatronic.class,
            ActorTablet.class, ActorDoorButton.class, ActorDoor.class, Nightroom.class, Void.class
        );
        
        ActorGameStateMenuMain actorGameStateMenuMain = new ActorGameStateMenuMain(this);
        this.doAddGameState(actorGameStateMenuMain);
        
        ActorGameStateModeSurvival actorGameStateModeSurvival = new ActorGameStateModeSurvival(this);
        this.doAddGameState(actorGameStateModeSurvival);

        ActorGameStateWin actorGameStateWin = new ActorGameStateWin(this);
        this.doAddGameState(actorGameStateWin);

        this.doSetActiveGameState("menu-main");
    }

    public void doAddGameState(ActorGameState actorGameState) {
        this.hashActorGameStates.put(actorGameState.name, actorGameState);
    }
    
    public void doSetActiveGameState(String name) {
        if (this.actorGameStateActive != null) {
            hashActorGameStates.get(this.actorGameStateActive).DoSetNotActive();
            this.removeObject(hashActorGameStates.get(this.actorGameStateActive));
        }
        this.actorGameStateActive = name;
        this.addObject(hashActorGameStates.get(name), 650, 350);
        hashActorGameStates.get(name).doSetAsActive();
    }
     
    public void doMusicPlay() {
        this.soundBackgroundMusic.playLoop();
    }
    
    public void doMusicStop(){
        this.soundBackgroundMusic.stop();
    }
}
