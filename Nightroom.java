import java.util.Random;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class Nightroom extends Actor
{
    Random random = new Random();

    static GreenfootImage imageOfficeNormal;
    static GreenfootImage imageOfficePowerOff;
    
    static GreenfootSound soundFlicker;
    
    static boolean statePowerOn = true;
    
    ActorAnimation actorAnimationFlicker;
    
    public Nightroom() {
        this.statePowerOn = true;

        this.imageOfficeNormal = new GreenfootImage("images/office-normal.png");
        this.imageOfficePowerOff = new GreenfootImage("images/office-power-off.png");

        this.setImage(this.imageOfficeNormal);
        
        String[] listStringImages = {
            "images/office-power-off.png", "images/office-normal.png", "images/office-power-off.png", "images/office-normal.png"
        };
        
        this.actorAnimationFlicker = new ActorAnimation(this, listStringImages);
        
        this.soundFlicker = new GreenfootSound("sounds/office/light-flicker.mp3");
    }

    protected void addedToWorld(World world) {
        Game game = (Game) getWorld();  
        game.addObject(this.actorAnimationFlicker, 0,0);        

    }    
    
    public void act() {
        this.doFlickerCheck();
    }
    
    public void doSetPowerOff() {
        this.statePowerOn = false;
        this.setImage(this.imageOfficePowerOff);
    }
    
    public void doFlickerCheck() {
       if (this.statePowerOn) {
            int int_random = random.nextInt(10000); 
            //System.out.println("[" + this.name + "] random: " + int_random);

            if (int_random <= 25) {
                //System.out.println("[" + this.name + "] Move");
                this.soundFlicker.play();
                this.actorAnimationFlicker.doAnimationTrigger();
            }            
        }        
        
    }
    
}
